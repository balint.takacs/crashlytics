library crashlytics;

import 'dart:async';

import 'package:crashlytics/repositories/crashlytics_repository.dart';
import 'package:crashlytics/utils/device.dart';
import 'package:crashlytics/utils/log_type.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

/// The entry point for accessing Crashlytics.
///
/// You can get an instance by calling [Crashlytics.instance].
class Crashlytics {
  static String? _crashlyticsHash;

  static Crashlytics? _instance;

  /// Returns a static instance of [Crashlytics], which you can use to easily access the functions of [Crashlytics].
  static Crashlytics get instance {
    if (_instance == null) {
      _instance = new Crashlytics();
    }
    return _instance!;
  }

  /// Before you can use Crashlytics, you must initialize it with this function.
  /// 
  /// You must call [WidgetsFlutterBinding.ensureInitialized()] before you initialize [Crashlytics].
  /// Ask mr.500 for the [apiKey] parameter.
  /// Config:
  /// 
  /// runZonedGuarded(
  ///   () async {
  ///     WidgetsFlutterBinding.ensureInitialized();
  ///     await Crashlytics.instance.init(apiKey: apiKey);
  ///     runApp(Application());
  ///   },
  ///   (error, stackTrace) {
  ///     Crashlytics.instance.logByStackTrace(trace: stackTrace, error: error);
  ///   },
  /// );
  Future<void> init({required String apiKey}) async {
    debugPrint("Crashlytics inited.");

    String? hash = await CrashlyticsRepository.init(apiKey);
    _crashlyticsHash = hash;

    if (hash != null) {
      FlutterError.onError = (FlutterErrorDetails details) {
        log(
            type: LogType.ERROR,
            log: "Exception: ${details.exception}\nStack: ${details.stack}");
      };
    } else {
      debugPrint("Failed to init.");
    }
  }

  Future<void> initAndRun({
    required String apiKey,
    required Widget application,
  }) async {
    runZonedGuarded(
      () async {
        WidgetsFlutterBinding.ensureInitialized();
        await Crashlytics.instance.init(apiKey: apiKey);
        runApp(application);
      },
      (error, stackTrace) {
        Crashlytics.instance.logByStackTrace(trace: stackTrace, error: error);
      },
    );
  }

  /// Send log to the [Crashlytics].
  Future<void> log({required LogType type, required String log}) async {
    assert(_crashlyticsHash != null);
    bool success = await CrashlyticsRepository.sendLog(
      crashId: _crashlyticsHash!,
      log: log,
      logType: type,
    );

    if (success) {
      debugPrint("[${DateTime.now()}] Log sent.");
    } else {
      debugPrint("[${DateTime.now()}] Failed to save log.");
    }
  }

  /// Send log to the [Crashlytics] by [StackTrace].
  Future<void> logByStackTrace(
      {Object? error, required StackTrace trace}) async {
    log(
      type: LogType.ERROR,
      log: "Exception: $error\nStack: $trace",
    );
  }
}
