library crashlytics;

import 'dart:convert';
import 'dart:io';

import 'package:crashlytics/crashlytics.dart';
import 'package:crashlytics/utils/device.dart';
import 'package:crashlytics/utils/log_type.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

/// This repository communicate with [Crashlytics] servers.
class CrashlyticsRepository {
  static const String _baseUrl = "crashlytics.appsolution.hu";

  /// This function will call server and generate a hash what you must use every API Call to authorize client.
  static Future<String?> init(String apiKey) async {
    String version = await Device.applicationVersion;

    try {
      var response = await http.post(
        Uri.https(_baseUrl, "/logger/init"),
        body: {
          "hash": apiKey,
          "os": Platform.isAndroid ? "ANDROID" : "IOS",
          "version": version,
          "deviceId": await Device.deviceId(),
        },
      );
      dynamic responseData = jsonDecode(response.body);
      if (responseData["message"] != "inited") {
        throw Exception(
          responseData["message"],
        );
      }
      return responseData["data"]["crashId"].toString();
    } catch (e) {
      print("Error: $e");
      return null;
    }
  }

  /// This function will send a log to server.
  static Future<bool> sendLog({
    required String crashId,
    required String log,
    required LogType logType,
  }) async {
    try {
      Map<String, dynamic> deviceData = await Device.getDeviceData();
      var response = await http.post(
        Uri.https(_baseUrl, "/logger/save"),
        body: {
          "crashId": crashId,
          "crashLog": log,
          "logType": logType.toString(),
          "deviceInfo": jsonEncode(deviceData),
        },
      ); 
      dynamic responseData = jsonDecode(response.body);
      return responseData["message"] == "saved";
    } catch (e) {
      print("Error: $e");
      return false;
    }
  }
}
