
/// If you use [Crashlytics.instance.log] function you must declare the type of Log.
enum LogType {
  WARNING,
  ERROR,
  CRASH,
  NAVIGATE,
  OTHER,
}
